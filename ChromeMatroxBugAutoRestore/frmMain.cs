﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace ChromeMatroxBugAutoRestore {
    public partial class FrmMain : Form {

        private string preferencesPath = "";
        private DateTime lastWriteTime = DateTime.MinValue;
        private int lastProcessCount = 0;

        public FrmMain() {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e) {
            preferencesPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            preferencesPath = Path.Combine(preferencesPath, "Google\\Chrome\\User Data\\Default\\Preferences");

            if (File.Exists(preferencesPath)) return;

            MessageBox.Show(@"Can't find your Google Chrome preferences file. Exiting...", @"Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            Close();
        }

        private void link_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            Process.Start("https://code.google.com/p/chromium/issues/detail?id=340972");
        }

        private void btnHide_Click(object sender, EventArgs e) {
            timer.Enabled = true;
            Hide();
        }

        private void timer_Tick(object sender, EventArgs e) {
            timer.Enabled = false;

            // Check if the preferences file has been updated.
            // This works in most cases, but the file doesn't get updated
            // if Chrome is closed while there are Chrome Apps running which
            // makes both Chrome and Chrome apps crash when Chrome is started again
            var writeTime = File.GetLastWriteTime(preferencesPath);
            if (writeTime > lastWriteTime) {
                FixPreferences();

                lastWriteTime = DateTime.Now;
                timer.Enabled = true;
                return;
            }

            // Additional check to see if the number of Chrome processes
            // has changed. Works ok for users who use Chrome Apps.
            var processCount = Process.GetProcesses().Count(x => x.ProcessName == "chrome");
            if (processCount != lastProcessCount) {
                FixPreferences();
                lastProcessCount = processCount;
            }

            timer.Enabled = true;
        }


        private void FixPreferences() {
            if (File.Exists(preferencesPath)) {
                File.Copy(preferencesPath, preferencesPath + ".ChromeMatroxBugAutoRestore.bak", true);

                var str = File.ReadAllText(preferencesPath);
                str = str.Replace("\"maximized\": true", "\"maximized\": false");
                File.WriteAllText(preferencesPath, str);

                Debug.WriteLine("Fixed Chrome Preferences");
            }
        }

        private void btnExit_Click(object sender, EventArgs e) {
            Close();
        }

    }
}
