Chrome Matrox Bug Auto Restore
==============================

This program lets you bypass [this](https://code.google.com/p/chromium/issues/detail?id=340972) Chrome/Matrox bug.

The program edits your Chrome Preferences file located at %AppData%\Local\Google\Chrome\User Data\Default\Preferences.

---

> Use this program at your own risk.
> You agree not to hold me liable for any damages or losses related to the use of this program.

